if vim.fn.exists('g:vscode') == 0 then
  require 'utils'
  require 'bindings'
  require 'settings'
  require 'plugins'

  -- source the old vimscript
  vim.cmd('source $HOME/.config/nvim/old-init.vim')
end
