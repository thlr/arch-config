""""""""""""""""""
" PLUGINS CONFIG "
""""""""""""""""""

" Go setup
" Run goimports along gofmt on each save
let g:go_fmt_command = "goimports"
" Automatically get signature/type info for object under cursor
let g:go_auto_type_info = 1

" NerdTree
nnoremap <leader>v <cmd> NERDTreeToggle<cr>
" Open if no file is specified, if vim is opened on a directory or close vim
" if nerdtree is the last tab open
autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Nerdtree syntax hilighting
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1

" lightline
let g:lightline = {
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'tab': {
    \   'active': [ 'tabnum', 'modified' ],
    \   'inactive': [ 'tabnum', 'modified' ]
    \ },
    \ 'tabline': {
    \   'right': [ ]
    \ },
    \ 'component_expand': {
    \   'buffers': 'lightline#bufferline#buffers'
    \ },
    \ 'component_type': {
    \   'buffers': 'tabsel'
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead'
    \ },
    \ }

"""""""""
" LOOKS "
"""""""""

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
colorscheme gruvbox

" Non transparent bg
" set background=dark

set signcolumn=yes:1

" Vim gutter (git integration)
" Use fontawesome icons as signs
let g:gitgutter_sign_added = ''
let g:gitgutter_sign_modified = ''
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = ''
let g:gitgutter_sign_modified_removed = ''
let g:gitgutter_hilight_lines = 0

" highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+\%#\@<!$/

" Override some colorscheme highlights for a neat transparent background
"hi Normal guibg=NONE ctermbg=NONE " background
"hi SignColumn guibg=NONE ctermbg=NONE " left most column
"hi LineNr guibg=NONE ctermbg=NONE " number colum
"hi CursorLineNr guibg=NONE ctermbg=NONE " number colum
"hi LightlineMiddle_tabline guibg=NONE ctermbg=NONE
