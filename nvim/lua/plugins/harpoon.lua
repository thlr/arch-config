require 'harpoon'.setup {
  global_settings = {
    save_on_toggle = false,
    save_on_change = true,
    enter_on_sendcmd = false,
    excluded_filetypes = { "harpoon" },
    mark_branch = true
  }
}

-- Manage marks
local ui = require('harpoon.ui')
vim.keymap.set('n', '<leader>a', require("harpoon.mark").add_file)
vim.keymap.set('n', '<C-e>', ui.toggle_quick_menu)
-- vim.keymap.set('n', '<leader>tc', require("harpoon.cmd-ui").toggle_quick_menu)

-- Navigate to existing marks
vim.keymap.set('n', '<C-j>', function() ui.nav_file(1) end)
vim.keymap.set('n', '<C-k>', function() ui.nav_file(2) end)
vim.keymap.set('n', '<C-l>', function() ui.nav_file(3) end)
vim.keymap.set('n', '<C-m>', function() ui.nav_file(4) end)

-- Terminals
-- local term = require('harpoon.term')
-- vim.keymap.set('n', '<leader>tu', function() term.gotoTerminal(1) end)
-- vim.keymap.set('n', '<leader>te', function() term.gotoTerminal(2) end)
-- vim.keymap.set('n', '<leader>cu', function() term.sendCommand(1, 1) end)
-- vim.keymap.set('n', '<leader>ce', function() term.sendCommand(1, 2) end)
