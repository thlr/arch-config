#!/usr/bin/env bash

# Cleans up the computer by deleting the various caches

if [ $UID != 0 ]; then
  echo "Please run me as super user"
  exit 1
fi

yes | docker system prune --volumes --all
nix-collect-garbage
yes | pacman -Scc
