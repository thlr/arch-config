require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "bash",
    "comment",
    "go",
    "jsdoc",
    "json",
    "bibtex",
    "latex",
    "lua",
    "markdown",
    "nix",
    "norg",
    "python",
    "regex",
    "toml",
    "vim",
    "yaml"
  },
  highlight = {
    enable = true,
    disable = { "bash" },
  },
  indent = {
    -- Indentation is bugged for most languages
    enable = true,
    disable = {"python", "yaml"}
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "<C-n>",
      node_incremental = "<C-n>",
      node_decremental = "<C-r>",
      scope_incremental = "<C-s>",
    },
  },
}
