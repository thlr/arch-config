-- general
vim.o.hidden = true
vim.o.clipboard = 'unnamedplus'
vim.o.showcmd = true
vim.o.mouse = 'a'
-- vim.o.wildmode = 'longest,list,full'
vim.o.colorcolumn = '80'

-- numbers
-- vim.o.rnu = true
vim.o.nu = true

--timings
vim.o.timeoutlen = 800
vim.o.updatetime = 100

-- splits
vim.o.splitbelow = true
vim.o.splitright = true

-- folding
vim.o.foldmethod = 'expr'
vim.o.foldlevel = 99

-- syntax highlighting
vim.o.syntax = 'on'

-- title
vim.o.title = true
vim.o.titlestring = '%F'
