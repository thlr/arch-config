-- Load plugins
require 'plugins.packer'

-- Plugins configuration
require 'plugins.treesitter'
require 'plugins.telescope'
require 'plugins.dap'
require 'plugins.neorg'
require 'plugins.tagbar'
require 'plugins.lspconfig'
require 'plugins.tabnine'
require 'plugins.nvim-cmp'
require 'plugins.vimtex'
require 'plugins.harpoon'
