local telescope = require 'telescope'
local builtin = require 'telescope.builtin'
local action_layout = 'telescope.actions.layout'

-- Builtins
vim.keymap.set("n", '<leader>ff', builtin.find_files)
vim.keymap.set("n", '<leader>fg', builtin.live_grep)
vim.keymap.set("n", '<leader>fG', builtin.grep_string)
vim.keymap.set("n", '<leader>fb', builtin.buffers)
vim.keymap.set("n", '<leader>fh', builtin.help_tags)
vim.keymap.set("n", '<leader>fk', builtin.keymaps)
vim.keymap.set("n", '<leader>fd',
  function()
    builtin.diagnostics({bufnr = 0})
  end
)

-- DAP extension
telescope.load_extension("dap")
vim.keymap.set("n", '<leader>fdc', telescope.extensions.dap.commands)
-- vim.keymap.set("n", '<leader>fdo', telescope.extensions.dap.configurations)
vim.keymap.set("n", '<leader>fdb', telescope.extensions.dap.list_breakpoints)
-- vim.keymap.set("n", '<leader>fdv', telescope.extensions.dap.variables)
vim.keymap.set("n", '<leader>fdf', telescope.extensions.dap.frames)

-- Lsp bindings
vim.keymap.set("n", 'gr', builtin.lsp_references)
vim.keymap.set("n", '<leader>fs', builtin.lsp_document_symbols)

-- Don't preview binaries
local previewers = require("telescope.previewers")
local Job = require("plenary.job")
local new_maker = function(filepath, bufnr, opts)
  filepath = vim.fn.expand(filepath)
  Job:new({
    command = "file",
    args = { "--mime-type", "-b", filepath },
    on_exit = function(j)
      local mime_type = vim.split(j:result()[1], "/")[1]
      if mime_type == "text" then
        previewers.buffer_previewer_maker(filepath, bufnr, opts)
      else
        -- maybe we want to write something to the buffer here
        vim.schedule(function()
          vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, { "BINARY" })
        end)
      end
    end
  }):sync()
end

telescope.setup {
  defaults = {
    buffer_previewer_maker = new_maker,
    mappings = {
      n = {
        ["<C-p>"] = action_layout.toggle_preview
      },
      i = {
        ["<C-u>"] = false,
        ["<C-p>"] = action_layout.toggle_preview,
      },
    },
    vimgrep_arguments = {
      'rg',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case'
    },
    prompt_prefix = "> ",
    selection_caret = "> ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "horizontal",
    layout_config = {
      horizontal = {mirror = false},
      vertical = {mirror = false}
    },
    file_sorter = require'telescope.sorters'.get_fuzzy_file,
    file_ignore_patterns = {
      '^.git/',
      '^.mypy_cache/',
      '^__pycache__/',
      '^output/',
      '%.ipynb',
    },
    generic_sorter = require'telescope.sorters'.get_generic_fuzzy_sorter,
    path_display = {},
    winblend = 0,
    border = {},
    borderchars = {'─', '│', '─', '│', '╭', '╮', '╯', '╰'},
    color_devicons = true,
    use_less = true,
    set_env = {['COLORTERM'] = 'truecolor'}, -- default = nil,
    file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
    grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
    qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,
  },
  pickers = {
    find_files = {
      find_command = { "fd", "--type", "f", "--strip-cwd-prefix" }
    },
  }
}
