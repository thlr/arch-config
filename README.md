# thlr's arch rice

Arch + dwm config

Terminal: st with zsh

Top bar made with dwmblocks

## Installing arch

Quick and opinionated guide for getting setup with an Arch install in minutes.
Disclaimer: this is especially made for my own setups and will most probably
not suit your needs. Start from a live usb.

TODO: script the disk encryption.

Load your keyboard setup

```{bash}
loadkeys fr
```

If you are using wifi, connect to the internet

```{bash}
iwctl
station wlan0 scan
station wlan0 connect <SSID>
ping 1.1.1.1 # check your connection
```

List your available disks

```{bash}
fdisk -l
```

Say you want to install on `/dev/sda`. Create the partitions

```{bash}
fdisk /dev/sda
```

Create partitions with `n` (I recommend creating them in this order):

- efi parition in front, if it does not exist - something like 512Mb. If it
    exists but belongs to windows, you should create a new one for linux.
- change efi type with `t` to EFI something something (alias `ef`)
- root partition
- swap partition, same size as the ram or less.
- home partition (optional)

Format the partitions (replace the values by their path):

```{bash}
mkfs.ext4 <root>
mkfs.ext4 <home> # If you created the partition
mkswap <swap>
mkfs.fat -F32 <efi> # If you created the partition
```

Mount the partitions:

```{bash}
mount <root> /mnt
mkdir -p /mnt/boot
mkdir /mnt/home
mount <efi> /mnt/boot
mount <home> /mnt/home # If you created a partition for it
swapon <swap>
```

Get the script, launch it and then follow the instructions.

```{bash}
pacman -S wget
wget https://gitlab.com/thlr/arch-config/-/raw/main/install_arch.sh
chmod +x ./install_arch.sh
./install_arch.sh
```

## TODO

- create post install script
- remove dependencies according to what's included in the base packages
- centralized place for setting the font
