vim.keymap.set("n", '<Space>', '<NOP>')

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- vim.keymap.set("n", '<C-h>', '<C-w>h', { remap = false })
-- vim.keymap.set("n", '<C-j>', '<C-w>j', { remap = false })
-- vim.keymap.set("n", '<C-k>', '<C-w>k', { remap = false })
-- vim.keymap.set("n", '<C-l>', '<C-w>l', { remap = false })

local function toggle_autocomment()
  local current_fo = vim.opt_local.fo:get()
  if current_fo.c and current_fo.r and current_fo.o then
    vim.opt_local.fo:remove({ 'c', 'r', 'o'})
  else
    vim.opt_local.fo:append('cro')
  end
end

vim.keymap.set("n", "<leader>c", toggle_autocomment)
vim.keymap.set("n", "<leader>i", function() vim.bo.autoindent = not vim.bo.autoindent end)

-- TODO move this in a command palette
vim.keymap.set("n", "<leader>sfr", ':setlocal spell! spelllang=fr<CR>')
vim.keymap.set("n", "<leader>sen", ':setlocal spell! spelllang=en_us<CR>')
