require 'neorg'.setup {
  load = {
    ["core.defaults"] = {},
    ["core.norg.dirman"] = {
      config = {
        workspaces = {
          notes = "~/sync/notes"
        },
        default_workspace = "notes"
      }
    },
    ["core.norg.concealer"] = {},
  }
}
