#!/usr/bin/env bash

# TODO journal size limit https://wiki.archlinux.org/title/Systemd/Journal#Journal_size_limit
# TODO packer

set -e

ZONE="Europe/Paris"
REFLECTOR_CONF="--country France --save /etc/pacman.d/mirrorlist --protocol https --latest 50 --sort rate" 
LOCALES="en_US.UTF-8 fr_FR.UTF-8"

[ $UID -ne 0 ] && echo "This script requires root privileges" && exit 1

ask_for_confirmation() {
  read -p "$1 [Y/n]" input
  if ! [ \( -z "$input" \) -o \( "$input" = "Y" -o "$input" = "y" \) ]
  then
    exit
  fi
}

cat <<EOF
This script will perform basic configuration for an arch system, and then
proceed to install the software that I commonly use.

No validation is performed on the inputs so enter them with care.
EOF

ask_for_confirmation "Do you wish to continue?"

echo
read -p "Enter a hostname: " HOSTNAME
read -s -p "Enter a root password (it will be used for your user account as well): " PASSWORD && echo
read -p "Enter a username: " USERNAME

cat <<EOF
Summary of the inputs:

hostname: $HOSTNAME
username: $USERNAME
password: ****

Locales: $LOCALES
Reflector configuration: $REFLECTOR_CONF
EOF

ask_for_confirmation "Do you want to proceed with the full installation?"

echo "Updating system clock"
timedatectl set-ntp true

echo "Installing Arch on /mnt"
pacstrap /mnt base linux linux-firmware

echo "Generating fstab"
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab
ask_for_confirmation "Does this look right to you?"

echo "Configuring the new system"
arch-chroot /mnt /bin/bash << EOF

echo "Setting up system clock"
ln -sf usr/share/zoneinfo/$ZONE /etc/localtime
hwclock --systohc
systemctl enable systemd-timesyncd.service

echo "Generating locales"
for locale in LOCALES; do
  sed -ie "s/^#\($locale\)/\1/" /etc/locale.gen
done
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=fr" > /etc/vconsole.conf

echo "Setting up the hostname"
echo $HOSTNAME > /etc/hostname
cat > /etc/hosts <<END
127.0.0.1	localhost
::1		localhost
127.0.1.1	$HOSTNAME.localdomain	$HOSTNAME
END

echo "Configuring the root account"
echo -e "$PASSWORD\n$PASSWORD" | passwd

echo "Setting up your user account"
useradd -m $USERNAME
echo -e "$PASSWORD\n$PASSWORD" | passwd $USERNAME
sed -ie "s/^\(root\)\(.*$\)/\1\2\n$USERNAME\2/" /etc/sudoers

echo "Setting up reflector"
yes | pacman -S reflector
echo "$REFLECTOR_CONF" > /etc/xdg/reflector/reflector.conf
systemctl enable reflector.service reflector.timer
systemctl start reflector.service reflector.timer

echo "Installing some basic programs"
yes | pacman -S dhcpcd grub efibootmgr ntfs-3g os-prober git networkmanager wget linux-headers base-devel

echo "Enabling NetworkManager"
systemctl enable NetworkManager

echo "Installing grub"
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

echo "Cloning the config repo"
git clone https://gitlab.com/thlr/arch-config.git /home/$USERNAME/config
EOF

echo "Unmounting partitions"
umount -R /mnt

echo "The system is ready. You can now reboot and proceed with the rest of the install (cd into ~/config)"
