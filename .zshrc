source "$HOME/.profile"
source "$HOME/.zgen/zgen.zsh"

# if the init script doesn't exist
if ! zgen saved; then

  # specify plugins here
  zgen oh-my-zsh
  zgen load zsh-users/zsh-syntax-highlighting

  # generate the init script from plugins above
  zgen save
fi

eval "$(starship init zsh)"
export STARSHIP_CONFIG=~/.config/starship/config.toml

[ -f ~/.aliasrc ] && source ~/.aliasrc

# kubectl and minikube autocompletion
source <(kubectl completion zsh)

# nice ls colors
source "$HOME/.local/share/lscolors.sh"

# lorri shell hook
eval "$(direnv hook zsh)"

# created by poetry for autocompletion
autoload -Uz compinit
zstyle ':completion:*' menu select
fpath+=~/.zfunc

set -o vi
# This binding needs to be re-set with vi mode
bindkey "^R" history-incremental-search-backward

eval "$(pyenv init -)"

# Unbind this for vim
bindkey -r "^J"

# Edit command
zle -N edit-command-line
bindkey -v '^V' edit-command-line

# terraform autocomplete
autoload -U +X bashcompinit && bashcompinit
complete -C /usr/bin/terraform terraform
complete -o nospace -C /usr/bin/terraform terraform

# Neat git TUI
lg()
{
    export LAZYGIT_NEW_DIR_FILE=~/.lazygit/newdir

    lazygit "$@"

    if [ -f $LAZYGIT_NEW_DIR_FILE ]; then
            cd "$(cat $LAZYGIT_NEW_DIR_FILE)"
            rm -f $LAZYGIT_NEW_DIR_FILE > /dev/null
    fi
}

if [ -e /home/thlr/.nix-profile/etc/profile.d/nix.sh ]; then . /home/thlr/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# Custom scripts
export PATH=$PATH:/home/thlr/bin

source '/home/thlr/lib/azure-cli/az.completion'
