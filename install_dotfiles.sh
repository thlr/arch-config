#!/usr/bin/env bash

[ $UID -eq 0 ] && echo "Don't run this script as super user!" && exit 1

CONFIG_PATH="$HOME/.config"
LOCAL_PATH="$HOME/.local"
LOCAL_FONTS="$LOCAL_PATH/share"

OVERRIDE=false
BECOME_ROOT=false
VERBOSE=false

print_help() {
    cat <<EOF
usage: ./install_config.sh [-f] [-h|--help]

    -f      Overrides existing config
    -e      Installs config that requires root access
    -h      Prints this help message
    -v      Verbose mode
EOF
}

while getopts "fhev" o; do
    case "$o" in
        f) OVERRIDE=true ;;
        h) print_help && exit ;;
        e) BECOME_ROOT=true ;;
        v) VERBOSE=true ;;
    esac
done

say() {
    if [ $VERBOSE = "true" ]; then
        echo $@
    fi
}

link_file() {
    # link relative file to selected directory
    target_path="$1"/"$2"
    [ -d "$2" ] && echo "link_file was called on a directory" 1>&2 && exit 1

    if [ -e "$target_path" ]; then
        if $OVERRIDE; then
            rm "$target_path"
        else
            say "$target_path Already exists. Use -f if you wish to override it." 1>&2
            return 1
        fi
    fi
    ln -sf "$PWD"/"$2" "$target_path"
    say "LINK \"$2\" => \"$target_path\""
}

link_path() {
    # $1 target path
    # $2 path to link under the target path.
    # For instance, link_path $HOME/.config dunst will link the entire dunst
    # directory under .config.
    # It is not destructive, if a dunst directory already exists in .config, it
    # will not be overriden. If a file exist already with the same name, it is
    # overriden only if specified. If there are other files that are not listed
    # under dunst, they will remain there.
    if [ -f "$2" ]; then
        link_file "$1" "$2"
    else
        mkdir -p "$1"/"$2"
        for subpath in "$2"/{.,}*; do
            subpath_basename="$(basename "$subpath")"
            if [ "$subpath_basename" == "." -o "$subpath_basename" == ".." ]; then
                continue
            fi
            link_path "$1" "$subpath"
        done
    fi
}

install_suckless_soft() {
    # Needs root permissions
    cd "$1"
    sudo make clean install &> /dev/null
    [ $? -ne 0 ] && echo "Failure when installing $1." && exit 1
    cd - &> /dev/null
    echo "Installed $1".
}

# .config directories
link_path $CONFIG_PATH dunst && echo "Configured dunst"
link_path $CONFIG_PATH jrnl && echo "Configured jrnl"
link_path $CONFIG_PATH nix && echo "Configured nix"
link_path $CONFIG_PATH nixpkgs && echo "Configured nixpkgs"
link_path $CONFIG_PATH rofi && echo "Configured rofi"
link_path $CONFIG_PATH zathura && echo "Configured zathura"
link_path $CONFIG_PATH starship && echo "Configured starship"
link_path $HOME .newsboat && echo "Configured newsboat"

# nvim
link_path $CONFIG_PATH nvim
mkdir -p $HOME/.venvs
(cd $HOME/.venvs && python -m venv debugpy && debugpy/bin/python -m pip install debugpy)
echo "Configured nvim"

# fonts
link_path $LOCAL_FONTS fonts && echo "Installed fonts"

# scripts
link_path $LOCAL_PATH bin && echo "Installed local scripts"

# Plain files
link_path $HOME .xinitrc && echo "Configured X11"

link_path $HOME .gitconfig && echo "Configured git"

configured_zsh="false"
link_path $HOME .aliasrc && configured_zsh="true"
link_path $HOME .kubectl_aliases && configured_zsh="true"
link_path $HOME .profile && configured_zsh="true"
link_path $HOME .zshrc && configured_zsh="true"
link_path $HOME .p10k.zsh && configured_zsh="true"
[ "$configured_zsh" = "true" ] && echo "Configured zsh"

link_path $HOME .tmux.conf && echo "Configured tmux"


# root config
if $BECOME_ROOT; then
    install_suckless_soft dwm
    install_suckless_soft dwmblocks
    install_suckless_soft st
    sudo sh -c "$(curl -fsSL https://starship.rs/install.sh)" -- -y &> /dev/null && echo "Installed starship prompt."
fi
