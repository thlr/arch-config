local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim',
    install_path })
end

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- libraries
  use 'nvim-lua/plenary.nvim'

  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'onsails/lspkind-nvim' -- vscode-like pictograms to neovim built-in lsp
  use 'lervag/vimtex' -- latex support

  -- Completion
  use 'hrsh7th/nvim-cmp' -- Completion engine
  use 'hrsh7th/cmp-buffer' -- nvim-cmp source for buffer words
  use 'hrsh7th/cmp-path' -- nvim-cmp source for paths
  use 'hrsh7th/cmp-nvim-lsp' -- Nvim-cmp source for neovim builtin LSP client
  use {
    'tzachar/cmp-tabnine',
    run = './install.sh'
  }
  use {
  	"windwp/nvim-autopairs",
      config = function() require("nvim-autopairs").setup {} end
  }

  -- Snippets
  use 'L3MON4D3/LuaSnip' -- snippet engine
  use 'saadparwaiz1/cmp_luasnip' -- luasnip completion source for nvim-cmp
  use 'honza/vim-snippets' -- snippets files for various programming languages.

  -- Syntax highlighing & more
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use 'towolf/vim-helm'

  -- eye candy
  use 'itchyny/lightline.vim'
  use 'ryanoasis/vim-devicons'

  -- nerdtree
  use 'preservim/nerdtree'
  use 'Xuyuanp/nerdtree-git-plugin'

  -- navigation
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/plenary.nvim'
    }
  }
  --use 'christoomey/vim-tmux-navigator'
  use 'ThePrimeagen/harpoon'

  -- git
  use 'airblade/vim-gitgutter'
  use 'tpope/vim-fugitive'

  -- debugging
  use 'mfussenegger/nvim-dap'
  use {
    'mfussenegger/nvim-dap-python',
    requires = { "mfussenegger/nvim-dap" }
  }
  use {
    'rcarriga/nvim-dap-ui',
    requires = { "mfussenegger/nvim-dap" }
  }
  use {
    'theHamsta/nvim-dap-virtual-text',
    requires = { "mfussenegger/nvim-dap" }
  }
  use {
    'nvim-telescope/telescope-dap.nvim',
    requires = {
      "mfussenegger/nvim-dap",
      "nvim-telescope/telescope.nvim"
    }
  }

  -- others
  use {
    'iamcco/markdown-preview.nvim',
    run = 'cd app & yarn install'
  }
  use 'preservim/tagbar'

  -- themes
  use 'morhetz/gruvbox'
  use 'rakr/vim-one'
  use 'joshdick/onedark.vim'
  use 'sts10/vim-pink-moon'
  use 'liuchengxu/space-vim-dark'
  use 'jacoborus/tender.vim'

  -- note taking
  use {
    "nvim-neorg/neorg",
    requires = "nvim-lua/plenary.nvim"
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
