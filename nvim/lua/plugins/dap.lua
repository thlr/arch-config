local dap, dapui, dap_python = require 'dap', require 'dapui', require 'dap-python'
require('nvim-dap-virtual-text').setup()

vim.keymap.set("n", "<F1>", dap.continue)
vim.keymap.set("n", "<F2>", dap.step_over)
vim.keymap.set("n", "<F3>", dap.step_into)
vim.keymap.set("n", "<F4>", dap.step_out)
vim.keymap.set("n", "<F5>", dap.terminate)
vim.keymap.set("n", "<leader>b", dap.toggle_breakpoint)
vim.keymap.set("n", "<leader>B", ":lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>")
-- vim.keymap.set("n", "<leader>lp", dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: ")))

dapui.setup({
  layouts = {
    {
      elements = {
        'console'
      },
      size = 10,
      position = 'bottom'
    },
    {
      elements = {
        'scopes',
        'breakpoints',
        'stacks',
        'watches'
      },
      size = 40,
      position = 'left'
    }
  },
})

-- auto open and close dapui with dap
dap.listeners.after.event_initialized["dapui_config"] = function()
  vim.keymap.set({"n", "v"}, "<leader>k", dapui.eval)
  vim.keymap.set("n", "<leader>r", ":lua require'dapui'.float_element('repl')<CR>")
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  -- dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  -- dapui.close()
end

dap_python.setup('~/.venvs/debugpy/bin/python')
dap_python.test_runner = 'pytest'
vim.keymap.set("n", "<leader>dn", dap_python.test_method, { remap = false })
vim.keymap.set("n", "<leader>df", dap_python.test_class, { remap = false })
vim.keymap.set("v", "<leader>ds", dap_python.debug_selection, { remap = false })
