# Android sdk related exports
#export ANDROID_HOME=$HOME"/Android/Sdk"
#export PATH=$PATH:$ANDROID_HOME"/emulator"
#export PATH=$PATH:$ANDROID_HOME"/tools"
#export PATH=$PATH:$ANDROID_HOME"/tools/bin"
#export PATH=$PATH:$ANDROID_HOME"/platform-tools"
#export PATH=$PATH:$ANDROID_HOME"/build-tools"

#export PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"
#export MANPATH="/home/linuxbrew/.linuxbrew/share/man:$MANPATH"
#export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:$INFOPATH"

# snap
export PATH="/var/lib/snapd/snap/bin/":$PATH

# Custom scripts
export PATH="$HOME/.local/bin/":$PATH
export PATH="$HOME/.local/bin/statusbar":$PATH

# Basics
export BROWSER=firefox
export EDITOR=nvim
export TERMINAL=st

# Kubernetes
export KUBECONFIG=$HOME/.kube/config
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Go
export PATH="$HOME/go/bin/":$PATH

# texlive
TEXLIVE_HOME="/usr/local/texlive/2020"
export MANPATH=$MANPATH:$TEXLIVE_HOME"/texmf-dist/doc/man"
export INFOPATH=$INFOPATH:$TEXLIVE_HOME"/texmf-dist/doc/info"
export PATH="/usr/local/texlive/2020/bin/x86_64-linux":$PATH

# Python
export PATH="$HOME/.poetry/bin:$PATH"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# Nix
if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then . $HOME/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# Rust applications
export PATH="$HOME/.cargo/bin:$PATH"

if [ -e /home/thlr/.nix-profile/etc/profile.d/nix.sh ]; then . /home/thlr/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# Ryax stuff
export RYAX_MAIN_DIR=$HOME/work/ryax
export NIX_PATH="ryaxpkgs=$RYAX_MAIN_DIR/ci_common/ryaxpkgs:$HOME/.nix-defexpr/channels:$NIX_PATH"
export NIXPKGS_ALLOW_INSECURE=1
